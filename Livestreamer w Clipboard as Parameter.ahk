SetTitleMatchMode 2

~F8::
	if( Copy_Twitch_URL() ){
		Start_Livestreamer_With_Clipboard()
	}
	return
	
~F9::
	Start_Livestreamer_With_Clipboard()
	return
	
~!F8::
	if( Copy_Twitch_URL() ){
		Start_Livestreamer_Audio_Only()
	}
	return

~!F9::
	Start_Livestreamer_Audio_Only()
	return
	
~^F8::
	if( Copy_Twitch_URL() ){
		
		winactivate, ahk_exe chrome.exe
		winwaitactive, ahk_exe chrome.exe
		sleep, 500
		send ^t
		sleep 350
		send ^v
		sleep 150
		send /chat
		send {Enter}
		sleep 150
		
		Start_Livestreamer_With_Clipboard()
	}
	return

~^F9::
	
	winactivate, ahk_exe chrome.exe
	winwaitactive, ahk_exe chrome.exe
	sleep, 500
	send ^t
	sleep 350
	send ^v
	sleep 150
	send /chat
	send {Enter}
	sleep 150
		
	Start_Livestreamer_With_Clipboard()
	
	return
	

Copy_Twitch_URL(){		;copies the URL of your current Twitch.tv window
	IfWinActive,Twitch
	{
		sleep 250
		SendInput,{f6}
		sleep 450
		Send ^c
		sleep 150
		Send ^w
		sleep 250
		return true
	}
	return false
}

Start_Livestreamer_Audio_Only(){
	Run "C:\Windows\System32\cmd.exe"
	
	WinWaitActive, cmd.exe, , .75
	Send_Stream_Command( "audio_only" )
}
	
Start_Livestreamer_With_Clipboard(){
	Run "C:\Windows\System32\cmd.exe"
	
	WinWaitActive, cmd.exe, , .75
	Send_Stream_Command()
	
	;	if VLC was not found/started then it tries to start Livestreamer with 720p60 Quality instead
	if( !Resize_VLC_Window() ){
		Send_Stream_Command( "720p60" )
		Resize_VLC_Window()
	}
	return
}

;	sends the actual line of text for the command line
Send_Stream_Command( streamQuality := "best" ){
	Send livestreamer{Space}
	Send !{Space}ep{Space}		;	pastes in the clipboard to the CMD line
	
	Send %streamQuality%{Enter}
	return
}

Resize_VLC_Window(){	;	resizes VLC if it is found, fails if VLC is not found
	
	WinWait, VLC,,3
	if ErrorLevel
	{
		return false
	}
	WinActivate, VLC,
	sleep 200
	
	WinMove,,VLC, 30, 30, 1440,810,
	WinSet, Region, 200-200 W200 H250, WinTitle
	
	return true
}