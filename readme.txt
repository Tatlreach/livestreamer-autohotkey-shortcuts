Created by Michael Salata
msalata89@gmail.com

Purpose: Start Livestreamer from Chrome with just a few hotkeys. 

Binds:
	Starting Livestreamer with the best quality stream
		F9		:		starts Livestreamer with the current clipboard as it's parameter
		F8		:		Copies the URL of Chrome current Twitch tab, closes tab, starts Livestreamer with clipboard
	
	Starting Livestreamer with a audio only stream
		Alt + F9		:		start Livestreamer with the clipboard as it's parameter
		Alt + F8		:		start Livestreamer with the URL of Chrome current tab, closes tab
	Opens a Twitch chat tab before opening a Livestreamer VLC stream
		Ctrl + F9		:		Utilizes existing Clipbard to open the chat and stream
		Ctrl + F8		:		Copies Twitch tab's URL from Chrome to open the chat and stream

	
NOTE: You may have to set the .exe to run in Administrative mode.

Setup:  Do One of the Below...
	A: Start the .exe file
	
	B: Double Click the .ahk file with Autohotkey installed
	
	C:	Add the .exe to your Startup folder.
		The Startup folder is located in %AppData%\Roaming\Microsoft\Windows\Start Menu\Programs folder.
		AppData folder can also be found by typing  %AppData% into your Windows Start Search bar.
		Add a shortcut to the .exe in the Startup folder
		Alternatively, you can move the .exe into the Startupfolder
		
	D: Create a Task that Runs on Startup
		Open Your Task Scheduler (type Task Scheduler into your windows search bar)
		Click - Create Task...  on the right hand side
			Navigate to the Actions Tab
				Click - New...
				Set Action to "Start a program"
				Set "Program/script:" to the location of the .exe
			Navigate to the Actions Tab
				Click - New...
				Set "Begin the task:" to  "At log on" or "At startup"